@ECHO off

set TARGET=www.bbc.co.uk
set LOGFILE=pingtest.txt
set INTERVAL=10

set SEP=++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
:LOOPSTART
ECHO Checking connection to %TARGET%, please wait...
echo. >>%LOGFILE%
echo %SEP% >> %LOGFILE%
echo %date% %time% Target = %TARGET% >> %LOGFILE%
echo %SEP% >> %LOGFILE%

ping %TARGET% -n 1 >> %LOGFILE%
echo %SEP% >> %LOGFILE%
echo. >> %LOGFILE%

timeout %INTERVAL%
GOTO LOOPSTART